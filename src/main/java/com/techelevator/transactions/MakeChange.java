package com.techelevator.transactions;

public class MakeChange {

	int bal;
	int newBal;
	int quarters;
	int nickels;
	int dimes;

	public void calculateChange(double bal) {

		newBal = (int)(bal);
		quarters = newBal / 25;
		newBal = newBal % 25;
		dimes = newBal / 10;
		newBal = newBal % 10;
		nickels = newBal / 5;

	}

	public int getQuarters() {
		return quarters;
	}

	public int getNickels() {
		return nickels;
	}

	public int getDimes() {
		return dimes;
	}



}
