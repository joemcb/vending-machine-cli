package com.techelevator.view;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import com.techelevator.transactions.Balance;
import com.techelevator.Writer;
import com.techelevator.products.Item;
import com.techelevator.transactions.MakeChange;

public class Menu {

	private PrintWriter out;
	private Scanner in;
	private Balance balance = new Balance();
	private MakeChange makeChange = new MakeChange();
	private Writer writer = new Writer();

	public Menu(InputStream input, OutputStream output) {
		this.out = new PrintWriter(output);
		this.in = new Scanner(input);

	}

	public void displayBanner() {
		out.println("*** Welcome to the Ultra-Vend 3000™ ***");
		out.println("     * Created by Orange Iguanas *");
	}

	public void displayMainMenu(List<Item> inventory) {
		while (true) {
			try {
				int choice = getMainMenu();
				if (choice == 1) {
					displayInventory(inventory);
				} else if (choice == 2) {
					purchaseMenu(inventory);
				} else if (choice > 2) {
					wrongChoiceMessage();
					continue;
				}
			} catch (InputMismatchException | IOException e) {
				wrongChoiceMessage();
				in.nextLine();
			}
		}
	}

	public void displayInventory(List<Item> inventory) {
		out.printf("%-2s   %-20s   %-6s   %-2s %-1s", "##", "Name", "Price", "Qty.", "\n");
		out.println("__________________________________________________");
		for (Item display : inventory) {
			out.printf("%-2s   %-20s   %-1s%2.2f   %-2s %-1s", display.getSlotNumber(), display.getItemName(), "$",
					display.getItemPrice(), display.getQuantityAsString(), "\n");
		}
		out.println("__________________________________________________");
		out.flush();
	}

	public int getMainMenu() {
		out.println("__________________________________________________");
		out.println("(1) Display Vending Machine Items");
		out.println("(2) Purchase");
		return getChoice();
	}

	public void purchaseMenu(List<Item> inventory) throws IOException {
		while (true) {
			out.println("__________________________________________________");
			out.println("(1) Feed Money");
			out.println("(2) Select Product");
			out.println("(3) Finish Transaction");
			out.println("(4) Main Menu");
			out.println("__________________________________________________");
			out.println("Current balance: " + balance.getBalanceFormatted());

			try {
				out.flush();
				String c = in.nextLine();
				out.flush();
				if (c.equalsIgnoreCase("1")) {
					feedMenu();
				} else if (c.equalsIgnoreCase("2")) {
					selectProductMenu(inventory);
				} else if (c.equalsIgnoreCase("3")) {
					finishTransaction();
				} else if (c.equalsIgnoreCase("4")) {
					break;
				} else {
					wrongChoiceMessage();
				}
			} catch (InputMismatchException e) {
				wrongChoiceMessage();

			}
		}

	}

	public void feedMenu() {
		while (true) {
			out.println("Insert desired amount...");
			out.println("__________________________________________________");
			out.println("(1) $1.00");
			out.println("(2) $2.00");
			out.println("(3) $5.00");
			out.println("(4) $10.00");
			out.println("(5) Return to previous menu");
			out.println("*** Current balance is: " + balance.getBalanceFormatted() + " ***");

			try {
				out.flush();
				String str = in.nextLine();
				out.flush();
				String startBal = balance.getBalanceFormatted();
				if (str.equals("1")) {
					balance.addMoney(100);
				} else if (str.equals("2")) {
					balance.addMoney(200);
				} else if (str.equals("3")) {
					balance.addMoney(500);
				} else if (str.equals("4")) {
					balance.addMoney(1000);
				} else if (str.equals("5")) {
					break;
				} else {
					wrongChoiceMessage();
				}
				String endBal = balance.getBalanceFormatted();
				String action = "FEED MONEY";
				writer.writeLog(action, startBal, endBal);
			} catch (InputMismatchException e) {
				wrongChoiceMessage();
				break;

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void selectProductMenu(List<Item> inventory) throws IOException {
		while (true) {
			out.println("Please enter your selection");
			out.println("Press '0' to return to previous menu");
			out.flush();
			String s = in.nextLine();
			out.flush();
			if (s.equals("0")) {
				break;
			} else {
				boolean slotExists = false;
				for (Item product : inventory) {
					if (product.getSlotNumber().equalsIgnoreCase(s)) {
						slotExists = true;
						processItem(product);
						break;
					}
				}
				if (!slotExists) {
					wrongChoiceMessage();
					break;
				}
			}
		}

	}

	public void processItem(Item product) throws IOException {
		while (true) {
			int quantity = product.getQuantity();
			int price = (int) (product.getItemPrice() * 100);
			int bal = balance.getBalance();

			if (quantity < 1) {
				soldOutMessage();
				break;
			} else if (price > bal) {
				notEnoughFundsMessage();
				break;
			} else {
				String startBal = balance.getBalanceFormatted();
				product.setQuantity(quantity - 1);
				balance.subtractMoney(price);
				out.println(product.getMessage());
				out.println("*** Current balance is: " + balance.getBalanceFormatted() + " ***");
				out.println("__________________________________________________");
				String endBal = balance.getBalanceFormatted();
				String action = product.getItemName() + " " + product.getSlotNumber();
				writer.writeLog(action, startBal, endBal);
				break;

			}
		}
	}

	public int getChoice() {
		out.flush();
		int n = in.nextInt();
		in.nextLine();
		return n;
	}

	public String getStringChoice() {
		out.flush();
		String s = in.nextLine();
		return s;
	}

	
	public void finishTransaction() throws IOException {
		String startBal = balance.getBalanceFormatted();
		makeChange.calculateChange((double) (balance.getBalance()));
		out.println("Your change is " + makeChange.getQuarters() + " quarters " + makeChange.getDimes() + " dimes "
				+ makeChange.getNickels() + " nickels.");
		balance.zeroBalance();
		out.println("Balance is: " + balance.getBalanceFormatted());
		String endBal = balance.getBalanceFormatted();
		String action = "Give Change";
		writer.writeLog(action, startBal, endBal);
	}

	public void wrongChoiceMessage() {
		out.println("");
		out.println("*****************");
		out.println("Invalid Selection");
		out.println("*****************");
		out.println();
		out.flush();
	}

	public void notEnoughFundsMessage() {
		out.println("");
		out.println("****************");
		out.println("Not enough funds");
		out.println("****************");
		out.println();
		out.flush();
	}

	public void soldOutMessage() {
		out.println("");
		out.println("****************");
		out.println("   Sold Out");
		out.println("****************");
		out.println();
		out.flush();
	}

	public void itemSoldMessage(String message) {
		out.println("");
		out.println("****************");
		out.println(message);
		out.println("****************");
		out.println();
		out.flush();
	}

}
