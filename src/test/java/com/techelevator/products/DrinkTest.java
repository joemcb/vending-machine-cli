package com.techelevator.products;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class DrinkTest {

	private String slotNumber = "C1";
	private String itemName = "Cola";
	private String itemPrice = "1.25";
	private String itemType = "Drink";
	private String quantity = "5";

	Drink drink = new Drink(slotNumber, itemName, itemPrice, itemType, quantity);

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void get_message_moonpie_returns_expected() {
		String result = drink.getMessage();
		Assert.assertEquals("Glug Glug, Yum!", result);

	}

}