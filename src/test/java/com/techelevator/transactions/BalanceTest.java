package com.techelevator.transactions;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class BalanceTest {

	Balance balance = new Balance();

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void add_money() {
		balance.addMoney(100);
		Assert.assertEquals(100, balance.getBalance());
	}

	@Test
	public void subtract_money() {
		balance.addMoney(100);
		balance.subtractMoney(20);
		Assert.assertEquals(80, balance.getBalance());
	}

	@Test
	public void formatted_balance() {
		balance.addMoney(250);
		Assert.assertEquals("$2.50", balance.getBalanceFormatted());
	}

	@Test
	public void zero_balance() {
		balance.addMoney(100);
		balance.zeroBalance();
		Assert.assertEquals(0, balance.getBalance());
	}

}
