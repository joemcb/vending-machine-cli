package com.techelevator.products;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class ChipTest {

	private String slotNumber = "A2";
	private String itemName = "Stackers";
	private String itemPrice = "1.45";
	private String itemType = "Chip";
	private String quantity = "5";

	Chip chip = new Chip(slotNumber, itemName, itemPrice, itemType, quantity);

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void get_message_stackers_returns_expected() {
		String result = chip.getMessage();
		Assert.assertEquals("Crunch Crunch, Yum!", result);

	}

}
