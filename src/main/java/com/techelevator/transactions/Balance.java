package com.techelevator.transactions;

import java.text.NumberFormat;


public class Balance {

	public int balance = 0;
	NumberFormat formatter = NumberFormat.getCurrencyInstance();
	public void addMoney(int amount) {
		balance = balance + amount;
	}

	public void subtractMoney(int amount) {
		balance = balance - amount;
	}

	public int getBalance() {
		return balance;
		}

	public String getBalanceFormatted() {
		Double dblBal = ((double)balance/100);
		return formatter.format(dblBal);
	}

	public void zeroBalance() {
		balance = 0;
	}

}
