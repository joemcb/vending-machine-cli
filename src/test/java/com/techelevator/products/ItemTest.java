package com.techelevator.products;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class ItemTest {

	private String slotNumber;
	private String itemName;
	private String itemPrice;
	private String itemType;
	private String quantity = "5";


	Item item = new Item(slotNumber, itemName, itemPrice, itemType, quantity);

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void set_quantity() {
		item.setQuantity(5);
		Assert.assertEquals(5, item.getQuantity());
	}

}
