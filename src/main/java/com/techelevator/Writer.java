package com.techelevator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Writer {

	public void writeLog(String action, String startBal, String endBal) throws IOException {

		String stringToWrite = "";
		File file = new File("Log.txt");
		FileWriter fileWriter = new FileWriter(file, true);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
		Date date = new Date(System.currentTimeMillis());

		if (!file.exists()) {
			file.createNewFile();

		}

		stringToWrite = formatter.format(date) + " " + action + "    " + startBal + "     " + endBal + "\n";

		bufferedWriter.write(stringToWrite);

		bufferedWriter.close();

	}
}
