package com.techelevator.products;

import java.text.NumberFormat;

public class Item {

	String slotNumber = "";
	String itemName = "";
	double itemPrice = 0;
	String itemType = "";
	int quantity = 0;
	NumberFormat formatter = NumberFormat.getCurrencyInstance();
	String message = "";


	public Item(String slotNumber, String itemName, String itemPrice, String itemType, String quantity) {
		this.slotNumber = slotNumber;
		this.itemName = itemName;
		this.itemPrice = Double.parseDouble(itemPrice);
		this.itemType = itemType;
		this.quantity = Integer.parseInt(quantity);

	}

	public String getQuantityAsString() {
		if (quantity < 1)
			return "Sold Out";
		return String.valueOf(quantity);
	}
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getSlotNumber() {
		return slotNumber;
	}

	public String getItemName() {
		return itemName;
	}

	public String getItemPriceAsString() {
		return formatter.format((itemPrice)/100);
	}
	public double getItemPrice() {
		return itemPrice;
	}


	public String getItemType() {
		return itemType;
	}
	public String getMessage() {
		return message;
	}

}
