package com.techelevator.products;

public class Gum extends Item {

	public Gum(String slotNumber, String itemName, String itemPrice, String itemType, String quantity) {
		super(slotNumber, itemName, itemPrice, itemType, quantity);
	}
	@Override
	public String getMessage() {
		return "Chew Chew, Yum!";
	}

}
