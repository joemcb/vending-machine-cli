package com.techelevator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.List;

import com.techelevator.products.Item;
import com.techelevator.view.Menu;

public class VendingMachineCLI {

	private Reader reader;
	private Menu menu;

	public VendingMachineCLI(Reader reader, Menu menu) {
		this.reader = reader;
		this.menu = menu;

	}

	public void run() throws IOException {

		List<Item> inventory = reader.readFile(); // Vending Machine is stocked

		menu.displayBanner();

		menu.displayMainMenu(inventory); //initialize menus
	}

	public static void main(String[] args) throws IOException {
		Reader reader = new Reader();
		Menu menu = new Menu(System.in, System.out);
		VendingMachineCLI cli = new VendingMachineCLI(reader, menu);
		cli.run();
	}
}
