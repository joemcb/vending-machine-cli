package com.techelevator.products;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class CandyTest {

	private String slotNumber = "B1";
	private String itemName = "Moonpie";
	private String itemPrice = "1.80";
	private String itemType = "Candy";
	private String quantity = "5";

	Candy candy = new Candy(slotNumber, itemName, itemPrice, itemType, quantity);

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void get_message_moonpie_returns_expected() {
		String result = candy.getMessage();
		Assert.assertEquals("Munch Munch, Yum!", result);

	}

}
