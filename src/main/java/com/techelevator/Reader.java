package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.techelevator.products.Candy;
import com.techelevator.products.Chip;
import com.techelevator.products.Drink;
import com.techelevator.products.Gum;
import com.techelevator.products.Item;

public class Reader {

	public List<Item> readFile() throws FileNotFoundException {
		String filePath = "vendingmachine.csv";
		List<Item> itemsList = new ArrayList<Item>();
		String slotNumber = "";
		String itemName = "";
		String itemPrice = "";
		String itemType = "";
		String quantity = "5";

		File file = new File(filePath);

		try (Scanner fileScanner = new Scanner(file)) {
			while (fileScanner.hasNextLine()) {
				String line = fileScanner.nextLine();
				String[] itemArray = line.split("\\|");
				slotNumber = itemArray[0];
				itemName = itemArray[1];
				itemPrice = itemArray[2];
				itemType = itemArray[3];

				if (itemType.equals("Chip")) {
					itemsList.add(new Chip(slotNumber, itemName, itemPrice, itemType, quantity));
				}
				if (itemType.equals("Candy")) {
					itemsList.add(new Candy(slotNumber, itemName, itemPrice, itemType, quantity));
				}
				if (itemType.equals("Drink")) {
					itemsList.add(new Drink(slotNumber, itemName, itemPrice, itemType, quantity));
				}
				if (itemType.equals("Gum")) {
					itemsList.add(new Gum(slotNumber, itemName, itemPrice, itemType, quantity));
				}

			}
		}
		return itemsList;
	}

}
