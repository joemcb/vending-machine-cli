package com.techelevator.transactions;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class MakeChangeTest {
	MakeChange makeChange = new MakeChange();



	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void dollar_amount_returns_correct_change() {
	makeChange.calculateChange(100);
	String result = makeChange.getQuarters()+ ", " +
	makeChange.getDimes() + ", " + makeChange.getNickels();
	Assert.assertEquals("4, 0, 0", result);
	}

	@Test
	public void dollar_amount_returns_correct_change_with_dimes() {
	makeChange.calculateChange(135);
	String result = makeChange.getQuarters()+ ", " +
	makeChange.getDimes() + ", " + makeChange.getNickels();
	Assert.assertEquals("5, 1, 0", result);
	}

	@Test
	public void dollar_amount_returns_correct_change_with_nickels() {
	makeChange.calculateChange(140);
	String result = makeChange.getQuarters()+ ", " +
	makeChange.getDimes() + ", " + makeChange.getNickels();
	Assert.assertEquals("5, 1, 1", result);
	}


}
