package com.techelevator.products;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class GumTest {

	private String slotNumber = "D1";
	private String itemName = "U-Chews";
	private String itemPrice = "0.85";
	private String itemType = "Gum";
	private String quantity = "5";

	Gum gum = new Gum(slotNumber, itemName, itemPrice, itemType, quantity);

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void get_message_u_chews_returns_expected() {
		String result = gum.getMessage();
		Assert.assertEquals("Chew Chew, Yum!", result);

	}

}
